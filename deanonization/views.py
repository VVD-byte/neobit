from django.views.generic.base import TemplateView
from django.shortcuts import render


class DeanonizationView(TemplateView):
    """
        Возвращает страницу с дианонизирующими дынными
    """
    template_name = 'deanonization/deanonization_page.html'
