import django_filters.rest_framework

from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, UpdateAPIView
from api import models, serializer


class FingerprintGetCreateView(ListCreateAPIView):
    """
        Создание и получение данных из таболицы FingerprintModel
        Filetr fields - Fingerprint
    """

    queryset = models.FingerprintModel.objects.all()
    serializer_class = serializer.FingerprintModelSerializer

    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['Fingerprint']

    def get(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class VisitGetCreateView(ListCreateAPIView):
    """
            Создание и получение данных из таболицы VisitsModel
            Filters fields - Fingerprint__Fingerprint, IP
    """

    queryset = models.VisitsModel.objects.all()
    serializer_class = serializer.VisitsModelSerializer

    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['Fingerprint__Fingerprint', 'IP']


class AddVisitIpView(UpdateAPIView):
    """
        Счетчик количества посещений по IP
        Принимает в query_params - Fingerprint__Fingerprint (фингерпринт)
        и в url принимает IP
    """

    queryset = models.VisitsModel.objects.all()
    serializer_class = serializer.AddVisitsIpSerializer

    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['Fingerprint__Fingerprint']

    lookup_field = 'IP'

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data={'Count': instance.Count + 1})
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class AddVisitDeviceView(UpdateAPIView):
    """
        Счетчик количества посещений по устройству
        В url принимает Fingerprint
    """

    queryset = models.FingerprintModel.objects.all()
    serializer_class = serializer.FingerprintModelSerializer

    lookup_field = 'Fingerprint'

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data={'Count': instance.Count + 1})
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
