from django.db import models


class FingerprintModel(models.Model):
    """
        Модель с колицеством посещений с определенного устройства (fingerprint)
    """
    Fingerprint = models.CharField(max_length=50, verbose_name='Цифровой отпечаток', blank=True, null=True)
    Count = models.PositiveIntegerField(verbose_name='Количество посещений', default=1)


class VisitsModel(models.Model):
    """
        Отношение IP - Fingerprint
    """
    IP = models.GenericIPAddressField(verbose_name='IP пользователя', blank=True, null=True)
    Fingerprint = models.ForeignKey(FingerprintModel,
                                    on_delete=models.CASCADE,
                                    verbose_name='Ссылка на цифровой отпечаток',
                                    blank=True,
                                    null=True)
    Count = models.PositiveIntegerField(verbose_name='Количество посещений', default=1)
    UseVpn = models.BooleanField(verbose_name='Исопльзуется ли vpn', default=False)
    UseProxy = models.BooleanField(verbose_name='Исопльзуется ли proxy', default=False)
    UseTor = models.BooleanField(verbose_name='Исопльзуется ли tor', default=False)
