#🖕 Fingerprint  
🎯 Цель  
Разработать веб-сайт проверки анонимности веб-браузера (аналог ipleak.com),
который позволит распознать пользователя даже после:  
  -смены браузера;  
  -настройки прокси/VPN/TOR.

☝ Распознать пользователя - означает, что при повторном заходе на
разработанный веб-сайт пользователь будет распознан как уже
посещавший его.

📝 Задачи  
Идентификация пользователя после смены IP и браузера.
Обнаружение факта использования прокси/VPN/TOR.
Обнаружение HTTP-заголовков прокси-серверов.

Интерфейс: достаточно сделать базовый веб с выводом информации о
деанонимизирующих параметрах.  

Задеплоить сервер на Google Cloud или аналоге (например, Google
Compute Engine на позволяет бесплатно разметить приложение на базовом
VDS).  

👉 Требования
Язык программирования на выбор: 🐹 Golang / 🐍 Python / 🤯 Node.js
Любые библиотеки

📜 Отчетность  
Веб-приложение с интерфейсом, выложенное на внешний сервер.  

⏳Срок  
2 недели