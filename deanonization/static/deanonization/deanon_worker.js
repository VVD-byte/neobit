var url_get_fingerprint = "api/fingerprint/?Fingerprint="  // get method возвращает данные о fingerprint
var url_post_fingerprint = "api/fingerprint/"  // post method создает и возвращает данные о fingerprint

var url_get_visit = "api/visit/?Fingerprint__Fingerprint="
var url_post_visit = "api/visit/"

var url_put_visit_ip = "api/add_visit_ip/"
var url_put_visit_device = "api/add_visit_device/"

window.onload = async function () {
    let ip = get_ip()
    let fingerprint = await get_fingerprint()
    get_data_visit(fingerprint, ip)
    add_visit_for_ip(fingerprint, ip)
    add_visit_for_device(fingerprint)
};

function requests_logs(method, url, data) {
    console.log({
        method: method,
        url: url,
        data: data
    })
}

///////

function write_data(data, fingerprint, ip) {
    let now_visit
    let i
    for (i = 0; i < data.length; i++) {
        if (data[i].Fingerprint.Fingerprint === fingerprint && data[i].IP === ip) {
            now_visit = data[i]
            break
        }
    }
    if (now_visit) {
        write_now_visit(now_visit)
        data.splice(i, i + 1)
    }
    if (data) {
        write_all_visits(data)
    }
}

function write_now_visit(data) {
    $('#count_f').html(data.Fingerprint.Count)
    $('#count_ip').html(data.Count)
    $('#id').html(data.Fingerprint.id)
    $('#proxy').html(data.UseProxy.toString())
    $('#vpn').html(data.UseVpn.toString())
    $('#tor').html(data.UseTor.toString())
}

function write_all_visits(data) {
    let page_text = ''
    for (let i = 0; i < data.length; i++) {
        page_text += '<div class="card mx-2 col-5" style="width: 18rem;">\n' +
            '  <div class="card-body">\n' +
            '<table className="table">' +
            '    <tbody>' +
            '    <tr>' +
            '        <td>IP</td>' +
            '        <td>' + data[i].IP + '</td>' +
            '    </tr>' +
            '    <tr>' +
            '        <td>Fingerprint</td>' +
            '        <td>' + data[i].Fingerprint.Fingerprint + '</td>' +
            '    </tr>' +
            '    <tr>' +
            '        <td>Количество визитов с данного устройства</td>' +
            '        <td>' + data[i].Fingerprint.Count + '</td>' +
            '    </tr>' +
            '    <tr>' +
            '        <td>Количество визитов с данного ip</td>' +
            '        <td>' + data[i].Count + '</td>' +
            '    </tr>' +
            '    <tr>' +
            '        <td>Использование ТОР</td>' +
            '        <td>' + data[i].UseTor.toString() + '</td>' +
            '    </tr>' +
            '    <tr>' +
            '        <td>Использование Proxy</td>' +
            '        <td>' + data[i].UseProxy.toString() + '</td>' +
            '    </tr>' +
            '    <tr>' +
            '        <td>Использование VPN</td>' +
            '        <td>' + data[i].UseVpn.toString() + '</td>' +
            '    </tr>' +
            '    </tbody>' +
            '</table>' +
            '  </div>\n' +
            '</div>\n'
    }
    $("#old_visit").html(
        page_text
    )
}

///////

function get_ip() {
    requests_logs('GET', "https://api.ipify.org?format=json", {})
    $.ajax({
        url: "https://api.ipify.org?format=json",
        method: "GET",
        async: false,
        success: function (data) {
            $("#ip").html(data.ip);
        }
    })
    return $("#ip").text()
}

async function get_fingerprint() {
    const fpPromise = import('https://openfpcdn.io/fingerprintjs/v3')
        .then(FingerprintJS => FingerprintJS.load())

    await fpPromise
        .then(fp => fp.get())
        .then(result => {
            console.log(result)
            $("#fingerprint").html(result.visitorId);
        })
    return $("#fingerprint").text()
}

///////

function get_data_visit(fingerprint, ip) {
    requests_logs('GET', location.href + url_get_visit + fingerprint, {})
    $.ajax({
        url: location.href + url_get_visit + fingerprint,
        method: "GET",
        async: false,
        success: function (data) {
            let all_ip = []
            for (let i = 0; i < data.length; i++) {
                all_ip.push(data[i]['IP'])
            }
            if (!all_ip.includes(ip)) {
                request_for_create_visit(fingerprint, ip)
            } else {
                write_data(data, fingerprint, ip)
            }
        }
    })
}

function request_for_create_visit(fingerprint, ip) {
    requests_logs(
        'POST',
        location.href + url_post_visit,
        {
            'IP': ip,
            'Fingerprint.Fingerprint': fingerprint,
        }
    )
    $.ajax({
        url: location.href + url_post_visit,
        method: "POST",
        async: false,
        headers: {'X-CSRFToken': $.cookie('csrftoken')},
        data: {
            'IP': ip,
            'Fingerprint.Fingerprint': fingerprint,
        },
        success: function (data) {
            get_data_visit(fingerprint, ip)
        }
    })
}

///////

function add_visit_for_ip(fingerprint, ip) {
    requests_logs(
        'PUT',
        location.href + url_put_visit_ip + ip + '?Fingerprint__Fingerprint=' + fingerprint,
        {}
    )
    $.ajax({
        url: location.href + url_put_visit_ip + ip + '?Fingerprint__Fingerprint=' + fingerprint,
        method: "PUT",
        async: false,
        headers: {'X-CSRFToken': $.cookie('csrftoken')},
    })
}

function add_visit_for_device(fingerprint) {
    requests_logs(
        'PUT',
        location.href + url_put_visit_device + fingerprint,
        {}
    )
    $.ajax({
        url: location.href + url_put_visit_device + fingerprint,
        method: "PUT",
        async: false,
        headers: {'X-CSRFToken': $.cookie('csrftoken')},
    })
}