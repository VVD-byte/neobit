FROM python:3.9.9-alpine3.15

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

RUN adduser --disabled-password neobit

WORKDIR /home/neobit

COPY requirements/prod.txt /home/neobit/requirements.txt
RUN pip install -r /home/neobit/requirements.txt && python -m pip install python-dotenv

COPY . /home/neobit

RUN chmod 755 -R /home/neobit \
    && chown neobit:neobit -R /home/neobit

ENTRYPOINT /home/neobit/docker_django-entrypoint.sh
