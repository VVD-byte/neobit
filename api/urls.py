from django.urls import path
from api import views


urlpatterns = [
    path('fingerprint/', views.FingerprintGetCreateView.as_view()),
    path('visit/', views.VisitGetCreateView.as_view()),
    path('add_visit_ip/<str:IP>', views.AddVisitIpView.as_view()),
    path('add_visit_device/<str:Fingerprint>', views.AddVisitDeviceView.as_view()),
]
