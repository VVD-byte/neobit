from django.urls import path
from deanonization import views


urlpatterns = [
    path('', views.DeanonizationView.as_view()),
]
