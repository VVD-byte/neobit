from rest_framework.serializers import ModelSerializer
from api import models


class FingerprintModelSerializer(ModelSerializer):

    class Meta:
        model = models.FingerprintModel
        fields = '__all__'


class VisitsModelSerializer(ModelSerializer):

    Fingerprint = FingerprintModelSerializer()

    class Meta:
        model = models.VisitsModel
        fields = '__all__'

    def create(self, validated_data):
        if validated_data.get('Fingerprint', {}).get('Fingerprint'):
            validated_data['Fingerprint'] = models.FingerprintModel.objects.get_or_create(
                Fingerprint=validated_data['Fingerprint']['Fingerprint'],
            )[0]
            validated_data['Fingerprint'].save()
        return super().create(validated_data)


class AddVisitsIpSerializer(ModelSerializer):

    class Meta:
        model = models.VisitsModel
        fields = '__all__'
