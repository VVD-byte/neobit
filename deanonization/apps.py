from django.apps import AppConfig


class DeanonConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'deanonization'
