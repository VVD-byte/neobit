from django.contrib import admin
from api import models


admin.site.register(models.VisitsModel)
admin.site.register(models.FingerprintModel)
